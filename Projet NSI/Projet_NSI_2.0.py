from tkinter import *
from tkinter.messagebox import *

# Création des différentes fonctions


def boutique_A(monnaie_a_rendre):
    billets = [500, 200, 100, 50, 20, 10, 5, 2, 1]
    monnaie_rendue = {}
    for i in range(len(billets)):
        nb_billets = monnaie_a_rendre // billets[i]
        monnaie_rendue[billets[i]] = nb_billets
        monnaie_a_rendre = monnaie_a_rendre % billets[i]
    for i in range(len(monnaie_rendue)):
        if monnaie_rendue[billets[i]] == 0:
            del monnaie_rendue[billets[i]]
    return f" Voici le résultat : {monnaie_rendue}"


def boutique_B(monnaie_a_rendre):
    billets = [200, 100, 50, 20, 10, 2]
    quantite_billets = [1, 3, 1, 1, 1, 5]
    billets_rendu = {}
    for i in range(len(billets)):
        nb_billets = 0
        while monnaie_a_rendre >= billets[i] and quantite_billets[i] > 0:
            monnaie_a_rendre -= billets[i]
            quantite_billets[i] -= 1
            nb_billets += 1
        billets_rendu[billets[i]] = nb_billets
    for i in range(len(billets_rendu)):
        if billets_rendu[billets[i]] == 0:
            del billets_rendu[billets[i]]
    return f" Voici le résultat : {billets_rendu}"


def boutique_C(noises, mornille=0, gallion=0):
    mornille = noises // 29 + mornille
    noises = noises % 29
    gallion = mornille // 17 + gallion
    mornille = mornille % 17
    return f" Le résultat est {noises} noises, {mornille} mornille(s) et {gallion} gallion(s)"


# Ces fonctions ne sont pas demandé mais servent à l'IHM et l'interface graphique

def message_console():
    showinfo('🌟 𝔹𝕠𝕦𝕥𝕚𝕢𝕦𝕖 𝕄𝕒𝕝𝕝𝕖 🌟', 'Dirigez vous sur la console pour continuer !')


def test_a():
    message_console()
    a = (int(input("Bienvenue chez Fleury et Bott, entrez la somme à rendre : ")))
    print(boutique_A(a))


def test_b():
    message_console()
    b = (int(input("Bienvenue chez Madame Guipure, entrez la somme à rendre : ")))
    print(boutique_B(b))


def test_c():
    message_console()
    c1 = (int(input("Bienvenue chez Ollivander, entrez le nombre de noise(s) : ")))
    c2 = (int(input("Entrez le nombre de mornille(s) : ")))
    c3 = (int(input("Entrez le nombre de gallions(s) : ")))
    print(boutique_C(c1, c2, c3))


# Création de la fênetre principale
screen = Tk()

# Personalisation de la fênetre
fond = PhotoImage(file='fond.png')
label = Label(screen, image=fond).pack()

screen.title("🌟 𝔹𝕠𝕦𝕥𝕚𝕢𝕦𝕖 𝕄𝕒𝕝𝕝𝕖 🌟")
screen.geometry("1080x720")
screen.maxsize(1080, 720)
screen.iconbitmap("logo.ico")
screen.config(background='#539284')

# Ajouts d'une boîte "frame", de titres et de sous-titres
frame = Frame(screen, bg='#FFFFFF', bd=5, relief=GROOVE)

ligne = Label(frame, text="*:–☆–:*:–☆–:*:–☆–:*:–☆–:*:–☆–:*:–☆–:*",
              font=("Arial", 25), bg='#FFFFFF', fg='black')
ligne.pack()

title = Label(frame, text="|🧰| 𝐇𝐚𝐫𝐫𝐲 𝐬𝐞 𝐟𝐚𝐢𝐭 𝐥𝐚 𝐦𝐚𝐥𝐥𝐞 ! |🧰|",
              font=("Arial", 35), bg='#FFFFFF', fg='black')
title.pack()

title2 = Label(frame, text="Choisi l'une des trois boutiques ! :",
               font=("Arial", 15), bg='#FFFFFF', fg='black')
title2.pack()

ligne2 = Label(frame, text="*:–☆–:*:–☆–:*:–☆–:*:–☆–:*:–☆–:*:–☆–:*",
               font=("Arial", 25), bg='#FFFFFF', fg='black')
ligne2.pack()

frame.place(x=210, y=100)

# Ajouts des différents boutons
store_a = Button(screen, text="★彡 Chez Fleury et Bott 彡★", font=("Arial", 15),
                 bg='white', fg='black', command=test_a)
# "commande" lance une fonction après l'execution du bouton
store_a.place(x=50, y=350, height=30, width=340)

store_b = Button(screen, text="★彡 Chez Madame Guipure 彡★", font=("Arial", 15),
                 bg='white', fg='black', command=test_b)
store_b.place(x=365, y=400, height=30, width=340)

store_c = Button(screen, text="★彡 Chez Ollivander 彡★", font=("Arial", 15),
                 bg='white', fg='black', command=test_c)
store_c.place(x=690, y=350, height=30, width=340)

# Ajouts des différentes photos
icone_a = PhotoImage(file='a.png')
label1 = Label(screen, image=icone_a, bg='#000')
label1.place(x=130, y=380)

icone_b = PhotoImage(file='b.png')
label2 = Label(screen, image=icone_b, bg='#000')
label2.place(x=430, y=430)

icone_c = PhotoImage(file='c.png')
label3 = Label(screen, image=icone_c, bg='#000')
label3.place(x=770, y=380)

# Affichage de la fênetre
screen.mainloop()
