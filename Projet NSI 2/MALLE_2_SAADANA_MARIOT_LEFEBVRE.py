# coding: utf-8

'''
Description du programme : effectue différents remplissages de malle
Auteurs : Adam SAADANA, Salomé LEFEBVRE, Loys MARIOT
Numéro de version FINALE
Date de dernière révision: 28/01/2022
'''
from random import randint


poids_maximal = 4
continuer = True
fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]


def n_importe_comment(liste_fournitures, poids_max):
    '''
    rempli la malle, peu importe avec quelles fournitures, sans dépasser le poids maximal

    entrées : liste_fournitures, table de dictionnaires contenant toutes les fournitures qu'un élève doit acheter
              poids_max, entier (ou flottant) du poids à ne pas dépassser
    sorties : contenu_malle, liste contenant les fournitures dans la malle
              poids_max, entier ou flottant du poids qu'il reste avant de dépasser le poids max
    '''
    poids_total = 0
    liste_finale = []
    for _ in liste_fournitures:
        i = randint(0, len(liste_fournitures) - 1)
        fourniture = liste_fournitures[i]
        if fourniture['Poids'] <= poids_max:
            poids_max -= fourniture['Poids']
            poids_total += fourniture['Poids']
            liste_finale.append(liste_fournitures[i]['Nom'])
        del liste_fournitures[i]
    return liste_finale, poids_total


def max_poids(liste_objets, poids_max):
    '''
    rempli la malle avec liste_objets en essayant d'avoir le poids total le plus
    élever possible sans dépasser le poids_max

    entrées:

    Sortie: la liste des nom des objets que l'on a mis dans la malle
    '''
    poids_total = 0
    poids_malle = 0
    objets_dans_malle = []
    noms_objets = [liste_objets[i]['Nom'] for i in range(len(liste_objets))]
    poids_objets = [liste_objets[i]['Poids'] for i in range(len(liste_objets))]
    while len(poids_objets) > 0:
        poids_min = poids_max + 1
        poids_restant = poids_max - poids_malle
        objet_enlever = []
        for j in range(len(poids_objets)):
            if poids_objets[j] > poids_restant:
                objet_enlever.append(j)
        for j in range(len(objet_enlever)-1, -1, -1):
            del poids_objets[objet_enlever[j]]
            del noms_objets[objet_enlever[j]]
        if len(poids_objets) == 0:
            continue
        for i in range(len(poids_objets)):
            if poids_objets[i] < poids_min:
                poids_min = poids_objets[i]
                index = i
        objets_dans_malle.append(noms_objets[index])
        poids_malle += poids_objets[index]
        poids_total += poids_objets[index]
        del poids_objets[index]
        del noms_objets[index]
    return objets_dans_malle, poids_total


def max_mana(liste_objets, poids_max):
    '''
    rempli la malle avec liste_objets en essayant d'avoir mana total le plus
    élever possible sans dépasser le poids_max

    entrées:

    Sortie: la liste des nom des objets que l'on a mis dans la malle
    '''

    poids_total = 0
    poids_malle = 0
    objets_dans_malle = []
    mana_objets = [liste_objets[i]['Mana'] for i in range(len(liste_objets))]
    noms_objets = [liste_objets[i]['Nom'] for i in range(len(liste_objets))]
    poids_objets = [liste_objets[i]['Poids'] for i in range(len(liste_objets))]
    while len(mana_objets) > 0:
        mana_max = 0
        poids_restant = poids_max - poids_malle
        objet_enlever = []
        nb_objets = len(mana_objets)
        for j in range(nb_objets):
            if poids_objets[j] > poids_restant:
                objet_enlever.append(j)
        if len(objet_enlever) > 0:
            for j in range(len(objet_enlever)-1, -1, -1):
                del mana_objets[objet_enlever[j]]
                del noms_objets[objet_enlever[j]]
                del poids_objets[objet_enlever[j]]
        if len(poids_objets) == 0:
            continue
        for i in range(len(mana_objets)):
            if mana_objets[i] > mana_max:
                mana_max = mana_objets[i]
                index_objet = i
            elif mana_objets[i] == mana_max and \
                poids_objets[i] < poids_objets[index_objet]:
                mana_max = mana_objets[i]
                index_objet = i
        objets_dans_malle.append(noms_objets[index_objet])
        poids_malle += poids_objets[index_objet]
        poids_total += poids_objets[index_objet]
        del mana_objets[index_objet]
        del noms_objets[index_objet]
        del poids_objets[index_objet]
    return objets_dans_malle, poids_total


while continuer == True:
    print("Comment voulez-vous remplir la malle ?")
    print("Pour la remplir n'apporte comment entrer A")
    print('Pour la remplir le plus lourdement possible entrer B')
    print('Pour remplir avec le maximum de mana enter C')
    print("Si vous voulez arréter entrer autre chose")
    comment = str(input("Alors que voulez-vous faire : "))
    if comment == 'A':
        malle = n_importe_comment(fournitures_scolaires, poids_maximal)
        print(f"Voici les objets dans la malle {malle[0]}")
        print(f"Avec un poids total de {malle[1]}")
    elif comment == 'B':
        malle = max_poids(fournitures_scolaires, poids_maximal)
        print(f"Voici les objets dans la malle {malle[0]}")
        print(f"Avec un poids total de {malle[1]}")
    elif comment == 'C':
        malle = max_mana(fournitures_scolaires, poids_maximal)
        print(f"Voici les objets dans la malle {malle[0]}")
        print(f"Avec un poids total de {malle[1]}")
    else:
        stop = str(input("Vous voulez arréter (oui ou non) : "))
        if stop == 'oui' :
            continuer = False
        else:
            continue